// Copyright (c) 2011-2017 The Cryptonote developers
// Copyright (c) 2014-2017 XDN developers
// Copyright (c) 2016-2017 BXC developers
// Copyright (c) 2017 Royalties developers
// Copyright (c) 2010-2017 Kohaku developers
// Copyright (c) 2017 Wayang developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#pragma once

namespace CryptoNote
{
  const static boost::uuids::uuid CRYPTONOTE_NETWORK = { { 0x30, 0xB2, 0xF4, 0x1A, 0x54, 0x45, 0x3e, 0xba, 0x34, 0xff, 0x00, 0x13, 0x00, 0x13, 0x55, 0xA3 } };
}
